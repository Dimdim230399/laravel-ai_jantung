<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMappingGejalaPenyakit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mapping_gejala_penyakit', function (Blueprint $table) {
            $table->increments('id_mapping');
            $table->string('kode_gejala',20);
            $table->string('kode_penyakit',20);
            $table->string('deskripsi_perawatan',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_mapping_gejala_penyakit');
    }
}
