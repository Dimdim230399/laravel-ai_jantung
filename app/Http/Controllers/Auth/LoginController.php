<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required|min:6'
        ]);

        $username = $request->input("username");
        $password = $request->input("password");
        $user = User::where("username", $username)->first();

        $data_user = [
            'username' => $user->username,
            'level' => $user->level,
            'email' => $user->email,
            'password' => $user->password,
        ];

        //  dd($user);
        // return response()->json($user->password);

        if (!$user) {
            $out = [
                "message" => "login_failed",
                "code"    => 401,
                "result"  => [
                    "token" => null,
                ]
            ];
            return response()->json($out, $out['code']);
        }

        if (Hash::check($password, $user->password)) {
            session::put('user_data', $data_user);
            if ($user->level == 'admin') {

                return redirect()->route('landing_admin');

            }elseif($user->level == 'user'){

                return redirect('user/');

            }
        } else {
            return redirect()->route('login')->with('error','Username And Password Are Wrong.');
        }
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:users|max:255',
            'password' => 'required|min:6',
            'username' => 'required',
            'level' => 'required'
        ]);

        $email = $request->input("email");
        $password = $request->input("password");
        $username = $request->input("username");
        $level = $request->input("level");

        $hashPwd = Hash::make($password);


        try {
            $user = new User;
            $user->email = $email;
            $user->password = $hashPwd;
            $user->username = $username;
            $user->level = $level;
            $user->save();
            // return generateJson("Register Berhasil");
            return redirect()->route('login')->with('error','Bener.');
        } catch (\Throwable $th) {
            // return generateJson("Gagall");
            return redirect()->route('register')->with('error','Username And Password Are Wrong.');

        }


    }

}
