<?php

namespace App\Http\Controllers\diagnosa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pasien;
use App\Models\Mapping;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Session;

class diagnosaController extends Controller
{
    public function Biodata(){
        return view('tampil_user.biodata');
    }

    public function awal(){
        return view('home');
    }



    function getSoal()
    {
        $query = DB::table("table_gejala as h")
            ->orderBy('kode_gejala', 'ASC')
            ->get();
            $data=[];
        foreach ($query as $key => $value) {
            $data[$key]['kode_gejala']=$value->kode_gejala;
            $data[$key]['nama_gejala']=$value->nama_gejala;
        }
        return view('tampil_user.diagnosa',compact('data'));
    }

    function postDataDiri(Request $request)
    {

// get soal / pertanyaan diagnosa
        // $query5 = DB::table("table_gejala as h")
        //     ->orderBy('kode_gejala', 'ASC')
        //     ->get();
        //     $soal=[];
        // foreach ($query5 as $key => $value) {
        //     $soal[$key]['kode_gejala']=$value->kode_gejala;
        //     $soal[$key]['nama_gejala']=$value->nama_gejala;
        // }
//  daftar chek up
        $insert = array(
            'hp' => $request->hp,
            'nama' => $request->nama,
            'usia' => $request->usia,
            'tinggi_badan' => $request->tinggi_badan,
            'berat_badan' => $request->berat_badan,
            'jenis_kelamin' => $request->jenis_kelamin
        );
        $pasien = new Pasien;
        $pasien->nama = $request->nama;
        $pasien->hp = $request->hp;
        $pasien->usia = $request->usia;
        $pasien->tinggi_badan = $request->tinggi_badan;
        $pasien->berat_badan = $request->berat_badan;
        $pasien->jenis_kelamin = $request->jenis_kelamin;
        $pasien->save();
        if ( $pasien->save() == true ){
            $data['code']="100";
            $data['message']="Sukses tambah data pasien!";
        } else {
            $data['code']="404";
            $data['message']="Gagal tambah data pasien!";
            return $data;
        }
        $dataPasien = Pasien::where('hp', $request->hp)->latest('created_at')->first();
        $insert = array(
            'id_pasien' => $dataPasien['id'],
            'created_at' => date("Y-m-d H:i:s")
        );
        $query = DB::table('table_history')->insert($insert);
        $dataHistory = DB::table('table_history as h')
                        ->selectraw('h.*, p.nama,p.hp,p.jenis_kelamin,p.tinggi_badan,p.berat_badan,p.usia')
                        ->leftJoin('pasien as p', 'h.id_pasien', '=', 'p.id')
                        ->where('id_pasien', $dataPasien['id'])->latest('created_at')->first();
        // dd($dataHistory);
        session::put('user_pasien', $dataHistory);
        return redirect()->route('soal');
    }

    function postJawaban(Request $request)
    {
        if ($request->kode_gejala == '' && $request->id_history=='') {
        } else {
                $insert = array(
                    'kode_gejala' => $request->kode_gejala,
                    'id_history' => $request->id_history
                );
                $query = DB::table('table_history_gejala')->insert($insert);
                if ( $query == true ){
                    $data['code']="100";
                    $data['message']="Sukses tambah data history gejala dan penyakit!";
                } else {
                    $data['code']="404";
                    $data['message']="Gagal tambah data history gejala dan penyakit!";
                }
                return $data;
        }
    }

    public function getHasil($id_history)
    {
        $hasil = DB::select("
        SELECT RAW.*, nama_penyakit, deskripsi_penyakit FROM
        (
        SELECT hasil.kode_penyakit,ROUND(point_of_pain/count_kp*100) AS presentase
        FROM
        (
            SELECT a.kode_penyakit, COUNT(a.kode_penyakit) AS point_of_pain
            FROM
            (
                SELECT hg.*, kode_penyakit
                FROM table_history_gejala hg, mapping_gejala_penyakit pgp
                WHERE hg.kode_gejala = pgp.kode_gejala
                AND id_history=?
            )a
            GROUP BY a.kode_penyakit
            ORDER BY point_of_pain DESC
            )hasil,
        (
            SELECT kode_penyakit,COUNT(kode_penyakit) AS count_kp FROM mapping_gejala_penyakit mgp
            GROUP BY kode_penyakit
            )raw
            WHERE hasil.kode_penyakit = raw.kode_penyakit
            )RAW,table_penyakit mp
            WHERE RAW.kode_penyakit = mp.kode_penyakit
            ORDER BY presentase DESC
            LIMIT 3
            ", [$id_history]);
        return $hasil;
    }

    public function getPerawatan($id_penyakit)
    {
        $hasil = DB::select('select deskripsi_perawatan as dp from mapping_gejala_penyakit where kode_penyakit = ?', [$id_penyakit]);
        return $hasil;
    }
}
