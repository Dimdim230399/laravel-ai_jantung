<?php

namespace App\Http\Controllers\Admin;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Models\Gejala;
use App\Models\Penyakit;
use App\Models\Mapping;
use DataTables;
use DB;
use Session;
class AdminController extends Controller
{
    // function __construct(){
    //     $user = session::get('user_data');
    //     if(isset($user)){
    //         $level = trim($user['level']);
    //     }
    //     if($level == 'user'){
    //         // return $next($request);
    //         $this->middleware('user', ['except' => ['getGejala']]) ; //exclude method
    //     }else{

    //         $this->middleware('admin', ['only' => ['getGejala', 'index']]); //selected method
    //     }

    // }

    public function index()
    {
        return view('layout_admin.main');
    }

   public function getGejala(Request $request)
    {
        $query = DB::table("table_gejala")
            ->orderBy('nama_gejala', 'ASC')
            ->get();
        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id_gejala']=$value->id_gejala;
            $data[$key]['nama_gejala']=$value->nama_gejala;
            $data[$key]['kode_gejala']=$value->kode_gejala;
        }
        $hasil = $data;
        // dd($hasil);
        if ($request->ajax()) {
            return DataTables::of($hasil)
                ->addColumn('action', function($data){
                    $button ='<a class="btn btn-xs btn-primary editData"
                    data-id_gejala="'.$data['id_gejala'].'" data-nama_gejala="'.$data['nama_gejala']
                    .'"data-kode_gejala="'.$data['kode_gejala'].'" href="javascript:void(0)"><i class="far fa-edit"></i></a>';
                    $button = $button.'<a class="btn btn-xs btn-danger deleteData"
                    data-id_gejala="'.$data['id_gejala'].'" href="javascript:void(0)"><i class="far fa-trash-alt"></i></a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('tampil_admin.gejala');

    }

   public function postGejala(Request $request)
    {
        $dataGejala = [
            'nama_gejala' => $request->nama_gejala,
            'kode_gejala' => $request->kode_gejala,
        ];
        Gejala::updateOrCreate(['id_gejala'=>$request->id_gejala], $dataGejala );
            $data['code']="100";
            if (isset($request->id_gejala) || $request->id_gejala=='') {
                $data['message']="Sukses Update data gejala!";
                # code...
            } else {
                $data['message']="Sukses tambah data gejala!";
                # code...
            }

        return $data;
    }

   public function postDGejala(Request $request)
    {
        $query = DB::table('table_gejala')
            ->where('id_gejala', $request['id_gejala'])->delete();
        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data gejala!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data gejala!";
        }
        return $data;
    }

    // penyakit
    function getPenyakit(Request $request)
    {
        $query = DB::table("table_penyakit")
        ->orderBy('nama_penyakit', 'ASC')
        ->get();
        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->id;
            $data[$key]['kode_penyakit']=$value->kode_penyakit;
            $data[$key]['nama_penyakit']=$value->nama_penyakit;
            $data[$key]['deskripsi_penyakit']=$value->deskripsi_penyakit;
        }

        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button ='<a class="btn btn-xs btn-primary editData" data-id="'.$data['id'].'"
                    data-nama_penyakit="'.$data['nama_penyakit'].'" data-kode_penyakit="'.$data['kode_penyakit'].'"
                    data-deskripsi_penyakit="'.$data['deskripsi_penyakit'].'"
                    href="javascript:void(0)"><i class="far fa-edit"></i></a>';
                    $button = $button.'<a class="btn btn-xs btn-danger deleteData"
                    data-id="'.$data['id'].'" href="javascript:void(0)"><i class="far fa-trash-alt"></i></a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('tampil_admin.penyakit');



    }


    function postPenyakit(Request $request)
    {
        $dataPenyakit = [
            'nama_penyakit' => $request->nama_penyakit,
            'kode_penyakit' => $request->kode_penyakit,
            'deskripsi_penyakit' => $request->deskripsi_penyakit,
            'created_by' => $request->created_by,
            'updated_by' => $request->updated_by,
        ];

        Penyakit::updateOrCreate(['id'=>$request->id], $dataPenyakit );
            if (isset($request->id) || $request->id!='') {
                $data['message']="Sukses Update data penyakit!";
                # code...
            } else {
                $data['message']="Sukses Tambah data penyakit!";
                # code...
            }
            $data['code']="100";
        return $data;
    }

    function postDPenyakit(Request $request)
    {
        $query = DB::table('table_penyakit')
            ->where('id', $request['id'])->delete();
        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data penyakit!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data penyakit!";
        }
        return $data;
    }



    // Mapping

    function getMapping(Request $request)
    {

        $query1 = DB::table("table_gejala")
        ->orderBy('nama_gejala', 'ASC')
        ->get();
        $data1 = array();
        foreach ($query1 as $key => $value) {
            $data1[$key]['id_gejala']=$value->id_gejala;
            $data1[$key]['nama_gejala']=$value->nama_gejala;
            $data1[$key]['kode_gejala']=$value->kode_gejala;
        }


        $query2 = DB::table("table_penyakit")
        ->orderBy('nama_penyakit', 'ASC')
        ->get();
        $data2 = array();
        foreach ($query2 as $key => $value) {
            $data2[$key]['id']=$value->id;
            $data2[$key]['kode_penyakit']=$value->kode_penyakit;
            $data2[$key]['nama_penyakit']=$value->nama_penyakit;
            $data2[$key]['deskripsi_penyakit']=$value->deskripsi_penyakit;
        }


        $query = DB::table("mapping_gejala_penyakit as m")
            ->selectRaw('m.*, g.nama_gejala as nama_g, p.nama_penyakit as nama_p')
            ->leftJoin('table_gejala as g', 'm.kode_gejala', '=', 'g.kode_gejala')
            ->leftJoin('table_penyakit as p', 'm.kode_penyakit', '=', 'p.kode_penyakit')
            ->orderBy('nama_g', 'ASC')
            ->get();
        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id_mapping']=$value->id_mapping;
            $data[$key]['kode_gejala']=$value->kode_gejala;
            $data[$key]['nama_g']=$value->nama_g;
            $data[$key]['kode_penyakit']=$value->kode_penyakit;
            $data[$key]['nama_p']=$value->nama_p;
            $data[$key]['deskripsi_perawatan']=$value->deskripsi_perawatan;
        }

        if ($request->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button ='<a href="javascript:void(0)" class="btn btn-xs btn-primary btnEdit"
                    data-id="'.$data['id_mapping'].'" data-kode_gejala="'.$data['kode_gejala']
                    .'"data-kode_penyakit="'.$data['kode_penyakit'].'"
                    data-deskripsi_perawatan="'.$data['deskripsi_perawatan'].'"><i class="far fa-edit"></i></a>';
                    $button = $button.'<a href="javascript:void(0)" class="btn btn-xs btn-danger
                    btnDelete" data-id="'.$data['id_mapping'].'"><i class="far fa-trash-alt"></i></a> ';

                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }



        return view('tampil_admin.mapping',compact('data1','data2'));


    }

    function postMapping(Request $request)
    {
        $insert = [
            'kode_gejala' => $request->kode_gejala,
            'kode_penyakit' => $request->kode_penyakit,
            'deskripsi_perawatan' => $request->deskripsi_perawatan,
        ];
        $query = Mapping::updateOrCreate(['id_mapping' => $request->id_mapping], $insert);
        $data['code']="100";
        if (isset($request->id_mapping) || $request->id_mapping!='') {
            $data['message']="Sukses Update data mapping gejala penyakit!";
            # code...
        } else {
            $data['message']="Sukses Tambah data mapping gejala penyakit!";
            # code...
        }
        return $data;
    }

    function postDMapping(Request $request)
    {
        $query = DB::table('mapping_gejala_penyakit')
            ->where('id_mapping', $request['id_mapping'])->delete();
        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data mapping!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data mapping!";
        }
        return $data;
    }





}
