<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $user = session::get('user_data');
        if(isset($user)){
            $level = trim($user['level']);
        }else{
            return redirect('/login')->with('error',"Anda Bukan Admin");
        }


        if($level == 'admin'){
            return $next($request);
        }

    return redirect(‘home’)->with(‘error’,"You don't have admin access.");
    }
}
