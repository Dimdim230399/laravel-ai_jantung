<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penyakit extends Model
{
    protected $table = 'table_penyakit';
    protected $primaryKey ='id';

      protected $fillable = [
        'kode_penyakit','nama_penyakit','deskripsi_penyakit'
    ];

}
