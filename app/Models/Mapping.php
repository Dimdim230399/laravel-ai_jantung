<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mapping extends Model
{
    protected $table = 'mapping_gejala_penyakit';
    protected $primaryKey ='id_mapping';

    protected $fillable = [
        'kode_gejala', 'kode_penyakit', 'deskripsi_perawatan'
    ];

}
