@extends('layouts.main')

@section('content')

<div class="page-header header-filter" data-parallax="true"
    style="background-image: url('{{asset('asset_user/img/bg-pakar.jpg')}}')">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="title">Sehatkah Jantung Kita?</h1>
                <h4>Hadir untuk membantu Anda memantau kesehatan Jantung<br>Anda.
                    Untuk sekarang dan masa yang akan datang,
                    untuk Anda dan generasi selanjutnya.</h4>
                <br>
                <a href="#pengertian" class="btn btn-success btn-raised btn-md">Mulai
                </a>
            </div>
        </div>
    </div>
</div>

<div class="main main-raised">
    <div class="container">
        <div class="section text-center" id="pengertian">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto">
                    <h2 class="title">Apa itu Penyakit Jantung?</h2>
                    <h4 class="description"><strong>Penyakit jantung adalah kondisi ketika jantung mengalami gangguan.
                            Bentuk gangguan itu sendiri bisa bermacam-macam. Ada gangguan pada pembuluh darah jantung,
                            irama jantung, katup jantung, atau gangguan akibat bawaan lahir.
                            <br>
                            Jantung adalah otot yang terbagi menjadi empat ruang. Dua ruang terletak di bagian atas,
                            yaitu atrium (serambi) kanan dan kiri. Sedangkan dua ruang lagi terletak di bagian bawah,
                            yaitu ventrikel (bilik) kanan dan kiri. Antara ruang kanan dan kiri dipisahkan oleh dinding
                            otot (septum) yang berfungsi mencegah tercampurnya darah yang kaya oksigen dengan darah yang
                            miskin oksigen.</strong></h4>
                </div>
            </div>

            <br>

            <div class="features">
                <div class="row">
                    <div class="col-md-4">
                        <div class="info">
                            <div class="icon icon-danger">
                                <i class="fas fa-medkit"></i>
                            </div>
                            <h3 class="info-title">100+</h3>
                            <h5>Cara perawatan terhadap hasil yang anda dapatkan.</h5>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info">
                            <div class="icon icon-success">
                                <i class="fas fa-check-circle"></i>
                            </div>
                            <h3 class="info-title">90%</h3>
                            <h5>Hasil yang ditampilkan mendekati kebenaran.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info">
                            <div class="icon icon-danger">
                                <i class="fas fa-medkit"></i>
                            </div>
                            <h3 class="info-title">100+</h3>
                            <h5>Cara perawatan terhadap hasil yang anda dapatkan.</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <a href="#tentang" class="btn btn-success btn-raised btn-md">Selanjutnya
                </a>
            </div>
        </div>

        <hr>

        <div class="section text-center" id="tentang">
            <h2 class="title">Tentang</h2>
            <br><br><br>
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-text card-header-danger">
                            <div class="card-text">
                                <h3 class="card-title">Apa - <br> penyebabnya?</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li class="float-left">Usia</li><br>
                                <li class="float-left">Obesitas</li><br>
                                <li class="float-left">Kurang olahraga</li><br>
                                <li class="float-left">Alkohol</li><br>
                                <li class="float-left">Keturunan</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-text card-header-warning">
                            <div class="card-text">
                                <h3 class="card-title">Apa tanda dan gejalanya?</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li class="float-left">Nyeri dada</li><br>
                                <li class="float-left">Mudah lelah</li><br>
                                <li class="float-left">Jantung berdebar-debar</li><br>
                                <li class="float-left">Gelisah, cemas, dan mudah lelah</li><br>
                                <li class="float-left">Sesak nafas saat beraktifitas</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-text card-header-success">
                            <div class="card-text">
                                <h3 class="card-title">Bagaimana mencegahnya?</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <ul>
                                <li class="float-left">Rutin cek kolesterol dan tekanan darah</li><br>
                                <li class="float-left">Pola Hidup Sehat</li><br>
                                <li class="float-left">Tidak merokok dan tidak minum alkohol</li><br>
                                <li class="float-left">Menjaga berat badan tubuh tetap ideal</li><br>
                                <li class="float-left">Istirahat yang cukup</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><br><br>
            <div>
                <a href="#last" class="btn btn-success btn-raised btn-md">Selanjutnya
                </a>
            </div>
        </div>
        <hr>

        <div class="section section-contacts" id="last">
            <br>
            <div class="col-md-8 ml-auto mr-auto">
                <h2 class="text-center title">Periksa Sekarang</h2>
                <br><br>
                <h4 class="text-center description">
                    Fitur utama pada web ini adalah sebuah sistem pakar yang dapat memberikan pengetahuan akan
                    kemungkinan
                    penyakit jantung yang ada di tubuh anda.
                    Setelah anda menekan tombol "mulai periksa" anda akan disuguhkan beberapa pertanyaan akan
                    gejala-gejala
                    penyakit jantung yang kemungkinan anda alami.
                    Isi sesuai dengan keadaan anda sekarang, dan lihat hasilnya!</h4>
                <br><br><br>
                <div class="col-md-4 ml-auto mr-auto text-center">
                    <a href="{{route('bio')}}" class="btn btn-success btn-raised btn-md">Mulai Periksa</a>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        // Initialize Tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Add smooth scrolling to all links in navbar + footer link
        $('a[href^="#"]').on('click', function (event) {

            // Make sure this.hash has a value before overriding default behavior


            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 900, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        })

    });
</script>

@endsection
