@extends('tampil_user.main')

@section('content')
<div class="container centerr" style="opacity: 0.8;">
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 ml-auto mr-auto">
                    <div class="card card-login">
                    <form class="form" method="POST" action="{{route('diagnosa')}}">
                            @csrf
                            <div class="card-header card-header-info text-center">
                                <h4 class="card-title">IDENTITAS ANDA</h4>
                                <div class="social-line">
                                    <a class="btn btn-just-icon btn-link">
                                        <i class="fas fa-phone"></i>
                                    </a>
                                    <a class="btn btn-just-icon btn-link">
                                        <i class="fas fa-heartbeat"></i>
                                    </a>
                                    <a class="btn btn-just-icon btn-link">
                                        <i class="fas fa-hospital-alt"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                {{-- usia usia tb tb bb --}}
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-user-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" id="nama"
                                        class="form-control @error('nama') is-invalid @enderror" name="nama"
                                        value="{{ old('nama') }}" required autocomplete="nama" autofocus
                                        placeholder="Nama Lengkap">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-group"></i>
                                        </span>
                                    </div>
                                    <div class="row ml-4">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" name="jenis_kelamin" value="Laki - Laki">
                                        Laki - Laki
                                        <span class="form-check-sign">
                                          <span class="check"></span>
                                        </span>
                                      </label>
                                      <label class="form-check-label ml-5">
                                        <input class="form-check-input" type="checkbox"  name="jenis_kelamin" value="Perempuan">
                                        Perempuan
                                        <span class="form-check-sign">
                                          <span class="check"></span>
                                        </span>
                                      </label>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" id="usia"
                                        class="form-control @error('usia') is-invalid @enderror" name="usia"
                                        value="{{ old('usia') }}" required autocomplete="usia" autofocus
                                        placeholder="Umur">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-phone"></i>
                                        </span>
                                    </div>
                                    <input type="text" id="hp" class="form-control @error('hp') is-invalid @enderror"
                                        name="hp" value="{{ old('hp') }}" required autocomplete="hp" autofocus
                                        placeholder="HP/Telepon">
                                </div>

                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-chart-line"></i>
                                        </span>
                                    </div>
                                    <input type="text" id="tinggi_badan" class="form-control @error('tinggi_badan') is-invalid @enderror"
                                        name="tinggi_badan" value="{{ old('tinggi_badan') }}" required autocomplete="tinggi_badan" autofocus
                                        placeholder="Tinggi Badan">
                                </div>

                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-sort-numeric-up"></i>
                                        </span>
                                    </div>
                                    <input type="text" id="berat_badan" class="form-control @error('berat_badan') is-invalid @enderror"
                                        name="berat_badan" value="{{ old('berat_badan') }}" required autocomplete="berat_badan" autofocus
                                        placeholder="Berat Badan">
                                </div>
                            </div>
                            <br>
                            <div class="footer text-center">
                                <button type="submit" class="btn btn-success btn-md" style="color: white;">Mulai</button>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
