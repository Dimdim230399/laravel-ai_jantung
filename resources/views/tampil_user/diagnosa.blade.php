@extends('tampil_user.main')
@php
$data_session = Session::get('user_pasien');
$idHistory = $data_session->id_history;
$urlEdit = url()->current();
@endphp
@section('content')
<div class="container centerr" style="opacity: 0.8;">
    <div class="divs">
        <form class="form" id="formJawaban" name="formJawaban" method="POST" action="{{ route('postJawaban') }}">
            @csrf
            <input type="text" id="kode_gejala" class="form-control" name="kode_gejala" value="">
            <input type="text" id="id_history" class="form-control" name="id_history" value="">
        </form>
        @foreach ($data as $key => $dt)
        <div class="card w-auto card_soal centerr" style="width: 90%">
            <div class="card-header card-header-info h3 text-center">Apakah anda merasakan {{$dt['nama_gejala']}} ?
            </div>
            <div class="card-body">
                <div class="cls{{$key}} ">
                    <div class="text-center">
                        <div class="row">
                            <div class="col-6">
                                <button class="btn btn-info next float-right" data-kode_gejala="{{$dt['kode_gejala']}}"
                                    data-id_history="{{$idHistory}}">
                                    YA
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
                            <div class="col-6">
                                <button class="btn btn-dark next float-left" data-kode_gejala="" data-id_history="">
                                    TIDAK
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="your-quiz-result"></div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="card w-auto centerr" id="card_hasil" style="width: 900px">
        <br>
        <br>
        <div class="card-header card-header-info h3 text-center">Hasil Analisa</div>
        <div class="card-body" style="height: 450px;overflow-y: auto;">
            <div class="cls{{$key}} ">

                <div class="row">
                    <div class="col-12">
                        <h3 class="title text-secondary mb-1 mt-3">Penyakit :</h3>
                    </div>
                    <div class="col-12" id="content_hasil">
                    </div>
                </div>
            </div>
            <div class="your-quiz-result"></div>
            <div class="col-md-4 ml-auto mr-auto text-center">
                <br>
            <a href="{{route('awal')}}" class="btn btn-success btn-round btn-md">Kembali Kemenu Awal</a>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript">
    $('.divs').attr('style', 'display:none');
    $('#formJawaban').attr('style', 'display:none');
    $('#card_hasil').attr('style', 'display:none');

    $(document).ready(function () {
        $('.divs').removeAttr('style');

        $(".divs > .card").each(function (e) {
            if (e != 0)
                $(this).hide();
        });
        // console.log('{{$urlEdit}}');
        $(".next").click(function (e) {
            e.preventDefault();
            if ($(this).html('IYA')) {
                $('#kode_gejala').val($(this).data('kode_gejala'));
                // console.log($('#kode_gejala').val());
                $('#id_history').val($(this).data('id_history'));
                // console.log($('#id_history').val());
                // console.log($('#formJawaban').serialize());
                $.ajax({
                    data: $('#formJawaban').serialize(),
                    url: "{{ route('postJawaban') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        // console.log(data);
                        $('#formJawaban').trigger('reset');
                    },
                    error: function (data) {
                        // console.log('Error:', data);
                    }
                });
            }


            console.log($(".divs .card:visible").next().length);

            if ($(".divs .card:visible").next().length == 0) {
                //Hide the two buttons
                $(".next, .prev").hide();
                $('.card_soal').attr('style', 'display:none');
                $('#card_hasil').removeAttr('style');

                var url = '{{$urlEdit}}/' + '{{$idHistory}}';
                console.log(url);
                console.log($('.penanganan').data('id_gejala'));
                var content = '';

                $.get(url, function (datas) {
                    $.each(datas, function (i, datas) {
                        console.log(datas.nama_penyakit);
                        content += '<hr><h4 class="title text-info mb-1 mt-0">' + datas.nama_penyakit + ' ' + datas.presentase + ' %</h4><p>"' +datas.deskripsi_penyakit +'"</p><h5 class="title text-info mb-1 mt-0">Penanganan : </h5><ul class="penanganan" data-id_gejala="'+datas.kode_penyakit+'"></ul>';
                        $('#content_hasil').html(content);

                        var url1 = '{{$urlEdit}}/rawat/' + datas.kode_penyakit;
                        console.log(url1);
                        $.get(url1, function (rawats) {
                            var perawatan = '';
                            $.each(rawats, function (i, rawats) {
                            perawatan += '<li>"'+rawats.dp+'"</li>';
                            $('.penanganan').html(perawatan);
                            });
                        });
                        return;
                    });
                });

                //Reverse the order
                var divs = jQuery.makeArray(divs);
                divs.reverse();
                $(".your-quiz-result")
                    .empty()
                    .append(divs);
            } else {
                $(".divs .card:visible").next().show().prev().hide();
            }
            return false;
        });

    });
</script>


@endsection
