<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.header')
<style>
    .centerr {
    margin: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%) }
</style>
</head>

<body>
    <div id="app">
        <main class="w-100 h-100" id="particles-js" style="position: absolute;">
            @yield('content')
        </main>
    </div>
</body>
@include('layouts.script')
</html>
