
<nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100"
id="sectionsNav">
<div class="container">
    <div class="navbar-translate">
    <a class="navbar-brand" href="">Sistem Pakar Jantung</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon"></span>
            <span class="navbar-toggler-icon"></span>
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" rel="tooltip" title="" data-placement="bottom"
                    href="#pengertian" target="_blank" data-original-title="Apa itu Covid ?">
                    <i class="fa fa-heartbeat"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" rel="tooltip" title="" data-placement="bottom"
                    href="#tentang" target="_blank" data-original-title="Tentang">
                    <i class="fa fa-question-circle"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" rel="tooltip" title="" data-placement="bottom"
                href="{{route('login')}}" target="_blank" data-original-title="Login Admin">
                    <i class="fa fa-question-circle"></i>
                </a>
            </li>

        </ul>
    </div>
</div>
</nav>
