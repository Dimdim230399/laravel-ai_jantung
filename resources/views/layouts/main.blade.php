<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.header')

    <style>
        .container1 {
          position: relative;
          width: 100%;
        }

        .image1 {
          display: block;
          width: 100%;
          height: auto;
        }

        .overlay1 {
          position: absolute;
          bottom: 0;
          left: 100%;
          right: 0;
          background-color: #008CBA;
          overflow: hidden;
          width: 0;
          height: 100%;
          transition: .5s ease;
        }

        .container1:hover .overlay1 {
          width: 100%;
          left: 0;
          opacity:0.8
        }

        .text1 {
          color: white;
          font-size: 20px;
          position: absolute;
          top: 50%;
          left: 50%;
          -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
        }

        /* actual styles */
        .slick-prev:before,
        .slick-next:before {
        color: rgb(85, 81, 81);
        font-size: 30px;
        }

        .slick-list {
            position: relative;
            display: block;
            overflow: hidden;
            margin: 15px;
            padding: 10px;
        }


        </style>
</head>

<body>
    @include('layouts.navbar')
    <!-- Main Content -->
    <!-- Content -->
    @yield('content')
</body>

@include('layouts.footer')
@include('layouts.script')

</html>
