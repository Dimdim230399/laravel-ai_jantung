<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="{{asset('asset_user/img/logo.png')}}">
<link rel="icon" href="{{asset('asset_user/img/logo.png')}}">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Sistem Pakar Jantung</title>
<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">


{{-- CSS BERITA --}}
{{-- <link rel="stylesheet" type="text/css" href="{{asset('slick/slick.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('slick/slick-theme.css')}}"/> --}}

<!-- CSS Files -->
<link href="{{asset('asset_user/css/material-kit.css?v=2.0.7')}}" rel="stylesheet" />
<!-- CSS Just for demo purpose, don't include it in your project -->
<link href="{{asset('asset_user/demo/demo.css')}}" rel="stylesheet" />




<script src="{{asset('assets/js1/jquery-3.5.1.js')}}"></script>
<!-- Fonts and icons -->
<script src="{{asset('assets/js/plugin/webfont/webfont.min.js')}}"></script>
{{-- char --}}
{{-- <script src="{{asset('assets/js/plugin/webfont/webfont.min.js')}}"></script>
<script src = "https://code.highcharts.com/highcharts.js" ></script>
<script src = "https://code.highcharts.com/modules/exporting.js"> </script>
<script src = "https://code.highcharts.com/modules/export-data.js" > </script>
<script src = "https://code.highcharts.com/modules/accessibility.js" > </script> --}}

<script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ["{{asset('assets/css/fonts.min.css')}}"]
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
</script>






