@extends('layout_admin.main')


@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div class="col-12 text-center">
                <h2 class="text-white pb-2 fw-bold">Selamat Datang</h2>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5 pb-0 w-100" style="position: absolute;">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-head-row card-tools-still-right">
                        <h4 class="card-title">Data Mapping</h4>
                        <div class="card-tools">
                            <button class="btn btn-primary btn-round ml-auto btnTambah" data-toggle="modal"
                                data-target="#modalgejala"><i class="fa fa-plus"></i>&nbsp Tambah Data</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="mapping" class="display table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th width="2%">No</th>
                                    <th width="8%">Nama Penyakit</th>
                                    <th width="8%">Nama Gejala</th>
                                    <th width="15%">Deskripsi Perawatan</th>
                                    <th width="2%" class="text-center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="title"></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formData" name="formData">
                    @csrf

                    <input id="id_mapping" hidden type="text" name="id_mapping" class="form-control" value="">

                    <div class="row">
                        <div class="col-md-6">
                            <label class="form-control-label mb-0" for="kode_penyakit">Penyakit</label>
                            <select id="kode_penyakit" name="kode_penyakit" class="form-control">
                                <option disabled selected>-&nbsp;Pilih Penyakit&nbsp;-</option>
                                @foreach ($data2 as $penyakit)
                                <option value="{{ $penyakit['kode_penyakit'] }}">&nbsp {{ $penyakit['nama_penyakit'] }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="form-control-label mb-0" for="kode_gejala">Gejala</label>
                            <select id="kode_gejala" name="kode_gejala" class="form-control">
                                <option disabled selected>-&nbsp;Pilih Gejala&nbsp;-</option>
                                @foreach ($data1 as $gejala)
                                <option value="{{ $gejala['kode_gejala'] }}">&nbsp {{ $gejala['nama_gejala'] }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row pt-2">
                        <div class="col-md-12">
                            <label class="form-control-label mb-0" for="deskripsi_perawatan">Deskripsi Perawatan</label>
                            <textarea id="deskripsi_perawatan" name="deskripsi_perawatan" class="form-control"></textarea>
                        </div>
                    </div>
            </div>
            <div class="modal-footer no-bd">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                <button type="submit" id="btnSave" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script>
    var table = $('#mapping').DataTable({
        "ordering": true,
        "order": [[ 1, 'ASC' ]],
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollCollapse": true,
        ajax: "{{ route('mapping') }}",
        columns: [
            {"data": "id",
                render: function (data, type, row, meta){
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data" : 'nama_p' },
            { "data" : 'nama_g' },
            { "data" : 'deskripsi_perawatan' },
            { "data" : "action", orderable: false, searchable: false },
        ]
    });

    $(document).ready(function() {
        $('.btnTambah').click(function() {
            $('#title').html('Tambah Data Mapping');
            $('#modal').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });
    });

    $('#btnSave').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');

        $.ajax({
            data: $('#formData').serialize(),
            url: "{{route('kelMap')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                console.log('Sukses');
                $('#formData').trigger('reset');
                $('#btnSave').html('Simpan');
                $('#modal').modal('hide');
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSave').html('Simpan');
            }
        });
    });

    $(document).on('click', '.btnEdit', function () {
        $('#title').html('Ubah Data Mapping');
        $('#id_mapping').val($(this).data('id'));
        $('#kode_gejala').val($(this).data('kode_gejala'));
        $('#kode_penyakit').val($(this).data('kode_penyakit'));
        $('#deskripsi_perawatan').val($(this).data('deskripsi_perawatan'));

        $('#modal').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })
    });

    $(document).on('click', '.btnDelete', function (e) {

        var id = $(this).data('id');

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'Yes, delete it!',
                    className: 'btn btn-success'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                $.ajax({
                    url: '{{url()->current()}}/' + $(this).data('id'),
                    type: "GET",
                    dataType: 'json',
                    success: function (data) {
                        console.log('Sukses hapus data: ', id);
                        table.draw();
                    },
                    error: function (data) {
                        console.log('Error: ', data);
                    }
                });
                swal({
                    title: 'Deleted!',
                    text: 'Your data has been deleted!',
                    type: 'success',
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    }
                });
            } else {
                swal.close();
            }
        });
    });
</script>

@endsection
