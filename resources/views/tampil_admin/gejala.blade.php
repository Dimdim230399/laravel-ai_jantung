@extends('layout_admin.main')

{{-- @php
$data_session = Session::get('user_data');
$namaUser = $data_session['name'];
@endphp --}}

@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div class="col-12 text-center">
                <h2 class="text-white pb-2 fw-bold">Selamat Datang</h2>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5 pb-0 w-100" style="position: absolute;">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card">
                {{-- atas --}}
                <div class="card-header">
                    <div class="card-head-row card-tools-still-right">
                        <h4 class="card-title">Data Gejala</h4>
                        <div class="card-tools">
                            <button class="btn btn-primary btn-round ml-auto btnTambah" data-toggle="modal"
                            data-target="#modalgejala"><i class="fa fa-plus"></i>&nbsp Tambah Data</button>
                        </div>
                    </div>
                </div>

                {{-- isi table --}}
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table_gejala" class="display table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th width="2%">No</th>
                                    <th width="5%">Kode Gejala</th>
                                    <th width="10%">Nama Gejala</th>
                                    <th width="2%" class="text-center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalgejala" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="title"></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formGejala"  name="formGejala">
                    @csrf
                    <input id="id_gejala" hidden type="text" name="id_gejala" class="form-control" value="">
                    <div class="row">
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-group-default">
                                <label>Kode Gejala</label>
                                <input id="kode_gejala" name="kode_gejala" type="text" class="form-control"
                                    placeholder="Contoh: G001">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-default">
                                <label>Nama Gejala</label>
                                <input id="nama_gejala" name="nama_gejala" type="text" class="form-control"
                                    placeholder="Contoh: Demam">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer no-bd">
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var table = $('#table_gejala').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollCollapse": true,
        ajax:{
            "url": "{{ route('gejala') }}",
            "type": "GET"
        },
        columns: [
            {
                data: "nomer",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            }, {
                data: 'kode_gejala',
                name: 'kode_gejala',
                searchable : false
            }, {
                data: 'nama_gejala',
                name: 'nama_gejala',
                orderable:false
            }, {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,

            },
        ]
    });

    $(document).on('click', '.editData', function () {
        console.log('sjfhdav');
            $('#title').html('Ubah Data Gejala');
            $('#kode_gejala').val($(this).data('kode_gejala'));
            $('#nama_gejala').val($(this).data('nama_gejala'));
            $('#id_gejala').val($(this).data('id_gejala'));

            $('#modalgejala').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });

    $(document).ready(function() {
        $('.btnTambah').click(function() {
            $('#title').html('Tambah Data Gejala');
            $('#modalgejala').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });



    });

    $('#btnSave').click(function (e) {
    e.preventDefault();
    $(this).html('Sending..');

        $.ajax({
            data: $('#formGejala').serialize(),
            url: "{{route('kelolagejala')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#formGejala').trigger('reset');
                $('#btnSave').html('Simpan');
                $('#modalgejala').modal('hide');
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSave').html('Simpan');
            }
        })
    });

    $(document).on('click', '.deleteData', function (e) {
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        buttons: {
            confirm: {
                text: 'Yes, delete it!',
                className: 'btn btn-success'

            },
            cancel: {
                visible: true,
                className: 'btn btn-danger'
            }
        }
    }).then((Delete) => {
        if (Delete) {
            console.log( $(this).data('id_gejala'));
            $.ajax({
                url: '{{url()->current()}}/' + $(this).data('id_gejala') ,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                    //$('#modalOrg').modal('show');
                }
            });
            swal({
                title: 'Deleted!',
                text: 'Your file has been deleted.',
                type: 'success',
                buttons: {
                    confirm: {
                        className: 'btn btn-success'
                    }
                }
            });
        } else {
            swal.close();
        }
    });
});
</script>
@endsection
