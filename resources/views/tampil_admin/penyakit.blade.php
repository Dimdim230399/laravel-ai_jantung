@extends('layout_admin.main')

@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div class="col-12 text-center">
                <h2 class="text-white pb-2 fw-bold">Selamat Datang</h2>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5 pb-0 w-100" style="position: absolute;">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card">
                {{-- atas --}}
                <div class="card-header">
                    <div class="card-head-row card-tools-still-right">
                        <h4 class="card-title">Data Penyakit</h4>
                        <div class="card-tools">
                            <button class="btn btn-primary btn-round ml-auto btnTambah" data-toggle="modal"
                                data-target="#modalpenyakit"><i class="fa fa-plus"></i>&nbsp Tambah Data</button>
                        </div>
                    </div>
                </div>
                {{-- isi table --}}
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table_penyakit" class="display table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="2%"">No</th>
                                    <th width="5%"">Kode Penyakit</th>
                                    <th width="15%"">Nama Penyakit</th>
                                    <th width="25%"">Deskripsi Penyakit</th>
                                    <th width="2%" class="text-center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalpenyakit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header no-bd">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="title"></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formPenyakit"  name="formPenyakit">
                    @csrf
                    <input id="id" hidden type="text" name="id" class="form-control" value="">
                    <div class="row">
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-group-default">
                                <label>Kode Penyakit</label>
                                <input id="kode_penyakit" name="kode_penyakit" type="text" class="form-control"
                                    placeholder="Contoh: P001">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-default">
                                <label>Nama Penyakit</label>
                                <input id="nama_penyakit" name="nama_penyakit" type="text" class="form-control"
                                    placeholder="Contoh: Jantung Koroner">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Deskripsi Penyakit</label>
                        <textarea class="form-control" id="deskripsi_penyakit" name="deskripsi_penyakit" rows="10"></textarea>
                    </div>
                </div>
                <div class="modal-footer no-bd">
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    <button type="button" id="btnSave" class="btn btn-primary">Tambah</button>
                </div>
            </form>

        </div>
    </div>
</div>



<script >

    var table = $('#table_penyakit').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollCollapse": true,
        ajax: "{{ route('penyakit') }}",
        columns: [
            {
                data: "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            }, {
                data: 'kode_penyakit',
                name: 'kode_penyakit',
                searchable: false
            }, {
                data: 'nama_penyakit',
                name: 'nama_penyakit',
                orderable: false,
            },{
                data: 'deskripsi_penyakit',
                name: 'deskripsi_penyakit',
                orderable: false,
                searchable: false
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },
        ]
    });

    $(document).on('click', '.editData', function () {
            $('#title').html('Ubah Data Penyakit');
            $('#kode_penyakit').val($(this).data('kode_penyakit'));
            $('#nama_penyakit').val($(this).data('nama_penyakit'));
            $('#deskripsi_penyakit').val($(this).data('deskripsi_penyakit'));
            $('#id').val($(this).data('id'));

            $('#modalpenyakit').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });

    $(document).ready(function() {
        $('.btnTambah').click(function() {
            $('#title').html('Tambah Data Penyakit');
            $('#modalpenyakit').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });



    });

    $('#btnSave').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
            data: $('#formPenyakit').serialize(),
            url: "{{route('kelolaPenyakit')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#formPenyakit').trigger('reset');
                $('#btnSave').html('Simpan');
                $('#modalpenyakit').modal('hide');
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSave').html('Simpan');
            }
        });
    });

    $(document).on('click', '.deleteData', function (e) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'Yes, delete it!',
                    className: 'btn btn-success'

                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                console.log( $(this).data('id'));
                $.ajax({
                    url: '{{url()->current()}}/' + $(this).data('id') ,
                    type: "GET",
                    dataType: 'json',
                    success: function (data) {
                        console.log('Sukses: ', data);
                        table.draw();
                    },
                    error: function (data) {
                        console.log('Error: ', data);
                    }
                });
                swal({
                    title: 'Deleted!',
                    text: 'Your file has been deleted.',
                    type: 'success',
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    }
                });
            } else {
                swal.close();
            }
        });
    });

</script>
@endsection
