<!DOCTYPE html>
<html lang="en">

<head>
    @include('layout_admin.header')
<style>
    .centerr {
    margin: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%) }
</style>
</head>

<body>

        @include('layout_admin.navbar')
        @include('layout_admin.sidebar')

        <!-- Main Content -->
        <div class="main-panel">

            <!-- Content -->
            <div class="content mb-5" id="particles-js" style="position: absolute;">
                @yield('content')
            </div>

            @include('layout_admin.footer')
        </div>


    @include('layout_admin.script')

</body>
</html>
