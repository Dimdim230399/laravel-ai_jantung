@php
$data_session = Session::get('user_data');
$namaUser = $data_session['username'];
$email = $data_session['email'];
@endphp
<div class="sidebar sidebar-style-2" data-background-color="white">
            <div class="sidebar-wrapper scrollbar scrollbar-inner">
                <div class="sidebar-content">
                    <div class="user">
                        <div class="avatar-sm float-left mr-2">
                            <img src="{{asset('assets/img/profile.jpg')}}" alt="..." class="avatar-img rounded-circle">
                        </div>
                        <div class="info">
                            <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                                <span>
                                    {{$namaUser}}
                                <span class="user-level"> {{$email}}</span>
                                <span class="caret"></span>
                                </span>
                            </a>
                            <div class="clearfix"></div>

                            <div class="collapse in" id="collapseExample">
                                <ul class="nav">
                                    <li>
                                        <a href="#profile">
                                            <span class="link-collapse">My Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#edit">
                                            <span class="link-collapse">Edit Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <span class="link-collapse">Logout</span>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-primary">
                        <li class="nav-section">
                            <span class="sidebar-mini-icon">
                                <i class="fa fa-ellipsis-h"></i>
							</span>
                            <h4 class="text-section">MENU</h4>
                        </li>
                        {{-- <li class="nav-item {{ (request()->is('')) ? 'active' : '' }} ">
                            <a href="">
                                <i class="fas phpdebugbar-fa-bar-chart"></i>
                                <p>Dash Boar</p>
                            </a>
                        </li> --}}
                         <li class="nav-item {{ (request()->is('*admin/*')) ? 'active' : '' }}">
                            <a data-toggle="collapse" href="#master">
                                <i class="fas fa-folder"></i>
                                <p>Master</p>
                                <span class="caret"></span>
                            </a>
                            <div class="collapse" id="master">
                                <ul class="nav nav-collapse">
                                    <li>
                                    <a href="{{route('gejala')}}">
                                            <i class="fas fa-bug"></i>
                                            <p>Gejala</p>
                                        </a>
                                    </li>
                                    <li>
                                    <a href="{{route('penyakit')}}">
                                            <i class="fas fa-procedures"></i>
                                            <p>Penyakit</p>
                                        </a>
                                    </li>
                                    <li>
                                    <a href="{{route('mapping')}}">
                                            <i class="fas fa-medkit"></i>
                                            <p>Mapping Rules</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        {{-- <li class="nav-item {{ (request()->is('*pasien')) ? 'active' : '' }} ">
                            <a href="{{ route('pasien') }}">
                                <i class="fas fa-user"></i>
                                <p>Pasien</p>
                            </a>
                        </li>
                        <li class="nav-item {{ (request()->is('*history/*')) ? 'active' : '' }}">
                            <a data-toggle="collapse" href="#history">
                                <i class="fas fa-clock"></i>
                                <p>History</p>
                                <span class="caret"></span>
                            </a>
                            <div class="collapse" id="history">
                                <ul class="nav nav-collapse">
                                    <li>
                                        <a href="{{ route('history') }}">
                                            <i class="fas fa-file-contract"></i>
                                            <p>History</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('hgejala') }}">
                                            <i class="fas fa-file-medical-alt"></i>
                                            <p>History Gejala</p>
                                        </a>
                                    </li> --}} --}}
                                    <!-- <li>
                                        <a href="">
                                            <i class="fas fa-file-medical"></i>
                                            <p>History Penyakit</p>
                                        </a>
                                    </li>                                     -->
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Sidebar -->
