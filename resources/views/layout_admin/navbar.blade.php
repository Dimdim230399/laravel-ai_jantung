

<!-- Header -->
<div class="main-header">
    <!-- Logo Header -->
    <div class="logo-header" data-background-color="blue2">
        <a class="logo" href="#"><img src="{{asset('asset_user/img/logoPakar.png')}}" class="navbar-brand py-0 my-0"
                width="150PX"></a>

        <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse"
            data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon">
                <i class="icon-menu"></i>
            </span>
        </button>
        <button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
        <div class="nav-toggle">
            <button class="btn btn-toggle toggle-sidebar">
                <i class="icon-menu"></i>
            </button>
        </div>
    </div>
    <!-- End Logo Header -->

    <!-- Navbar Header -->
    <nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue">

        {{-- @if(request()->is('')) --}}
        <div class="container-fluid">
            <div class="navbar-left mr-md-3 text-white">Halaman Admin</div>
        </div>
        {{-- @endif --}}
        {{--
            @if(request()->is(''))
            <div class="container-fluid">
                <div class="navbar-left mr-md-3 text-white">Halaman Pengelolaan History Pasien</div>
            </div>
            @endif

            @if(request()->is(''))
            <div class="container-fluid">
                <div class="navbar-left mr-md-3 text-white">Halaman Pengelolaan History Gejala</div>
            </div>
            @endif

            @if(request()->is(''))
            <div class="container-fluid">
                <div class="navbar-left mr-md-3 text-white">Halaman Pengelolaan Master Penyakit</div>
            </div>
            @endif

            @if(request()->is(''))
            <div class="container-fluid">
                <div class="navbar-left mr-md-3 text-white">Halaman Pengelolaan Master Gejala</div>
            </div>
            @endif

            @if(request()->is('*mapping*'))
            <div class="container-fluid">
                <div class="navbar-left mr-md-3 text-white">Halaman Pengelolaan Mapping Gejala Penyakit</div>
            </div>
            @endif

            @if(request()->is('*landing'))
            <div class="container-fluid">
                <div class="navbar-left mr-md-3 text-white">Halaman Statistik Penyakit</div>
            </div>
            @endif --}}
    </nav>
    <!-- End Navbar -->
</div>
