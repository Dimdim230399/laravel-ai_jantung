/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.11-MariaDB : Database - jantung
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`jantung` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `jantung`;

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `mapping_gejala_penyakit` */

DROP TABLE IF EXISTS `mapping_gejala_penyakit`;

CREATE TABLE `mapping_gejala_penyakit` (
  `id_mapping` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode_gejala` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_penyakit` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi_perawatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_mapping`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `mapping_gejala_penyakit` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_08_19_000000_create_failed_jobs_table',1),
(4,'2020_10_24_125950_create_table_gejala',2),
(5,'2020_10_24_131835_create_table_penyakit',3),
(6,'2020_10_24_132116_create_table_mapping_gejala_penyakit',3),
(7,'2020_10_24_133037_create_pasien',3),
(8,'2020_10_24_133111_create_table_history',3),
(9,'2020_10_24_133207_create_table_history_gejala',3),
(10,'2020_10_24_134329_create_table_gejala',4);

/*Table structure for table `pasien` */

DROP TABLE IF EXISTS `pasien`;

CREATE TABLE `pasien` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hp` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tinggi_badan` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `berat_badan` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usia` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `pasien` */

insert  into `pasien`(`id`,`hp`,`nama`,`tinggi_badan`,`berat_badan`,`usia`,`jenis_kelamin`,`created_at`,`updated_at`) values 
(2,'08936321','Dimas','170','60','21','Laki - Laki','2020-10-25 03:11:42','2020-10-25 03:11:42'),
(3,'08936321','Dimas','170','60','21','Laki - Laki','2020-10-25 03:29:31','2020-10-25 03:29:31'),
(4,'08936321','Dimas','170','60','21','Laki - Laki','2020-10-25 03:31:29','2020-10-25 03:31:29'),
(5,'08936321','Dimas','170','60','21','Laki - Laki','2020-10-25 03:42:54','2020-10-25 03:42:54'),
(6,'00023','Dimas','120','70','20','Laki - Laki','2020-10-25 03:56:37','2020-10-25 03:56:37'),
(7,'00023','Dimas','120','70','20','Laki - Laki','2020-10-25 03:57:10','2020-10-25 03:57:10'),
(8,'00023','Dimas','120','70','20','Laki - Laki','2020-10-25 03:58:19','2020-10-25 03:58:19'),
(9,'00023','Dimas','120','70','20','Laki - Laki','2020-10-25 04:03:11','2020-10-25 04:03:11'),
(10,'00023','Dimas','120','70','20','Laki - Laki','2020-10-25 04:08:08','2020-10-25 04:08:08'),
(11,'00023','Dimas','120','70','20','Laki - Laki','2020-10-25 04:11:47','2020-10-25 04:11:47'),
(12,'00023','Dimas','120','70','20','Laki - Laki','2020-10-25 04:17:13','2020-10-25 04:17:13'),
(13,'00023','Dimas','120','70','20','Laki - Laki','2020-10-25 04:18:20','2020-10-25 04:18:20'),
(14,'00023','Dimas','120','70','20','Laki - Laki','2020-10-25 04:19:09','2020-10-25 04:19:09'),
(15,'00023','Dimas','120','70','20','Laki - Laki','2020-10-25 04:19:14','2020-10-25 04:19:14'),
(16,'00023','Dimas','120','70','20','Laki - Laki','2020-10-25 04:19:39','2020-10-25 04:19:39'),
(17,'00023','Dimas','170','60','21','Laki - Laki','2020-10-25 04:20:03','2020-10-25 04:20:03'),
(18,'00023','Dimas','170','60','21','Laki - Laki','2020-10-25 04:21:54','2020-10-25 04:21:54'),
(19,'00023','Dimas','170','60','21','Laki - Laki','2020-10-25 04:22:38','2020-10-25 04:22:38'),
(20,'00023','Dimas','170','60','21','Laki - Laki','2020-10-25 04:23:34','2020-10-25 04:23:34'),
(21,'00023','Dimas','170','60','21','Laki - Laki','2020-10-25 04:25:18','2020-10-25 04:25:18'),
(22,'00023','Dimas','170','60','21','Laki - Laki','2020-10-25 04:26:17','2020-10-25 04:26:17'),
(23,'00023','Dimas','170','60','21','Laki - Laki','2020-10-25 04:28:16','2020-10-25 04:28:16'),
(24,'00023','Dimas','170','60','21','Laki - Laki','2020-10-25 04:29:41','2020-10-25 04:29:41'),
(25,'00023','Dimas','170','60','21','Laki - Laki','2020-10-25 04:30:13','2020-10-25 04:30:13'),
(26,'00023','asfdasfad','170','60','21','Perempuan','2020-10-25 05:33:14','2020-10-25 05:33:14'),
(27,'00023','asfdasfad','170','60','21','Perempuan','2020-10-25 05:34:25','2020-10-25 05:34:25'),
(28,'00023','asfdasfad','170','60','21','Perempuan','2020-10-25 05:37:52','2020-10-25 05:37:52'),
(29,'00023','asfdasfad','170','60','21','Perempuan','2020-10-25 05:38:21','2020-10-25 05:38:21'),
(30,'00023','asfdasfad','170','60','21','Perempuan','2020-10-25 05:45:19','2020-10-25 05:45:19'),
(31,'00023','asfdasfad','170','60','21','Perempuan','2020-10-25 05:45:49','2020-10-25 05:45:49'),
(32,'00023','Dimas','170','60','20','Laki - Laki','2020-10-25 06:22:25','2020-10-25 06:22:25'),
(33,'00023','dim','170','60','20','Laki - Laki','2020-10-25 06:24:26','2020-10-25 06:24:26');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `table_gejala` */

DROP TABLE IF EXISTS `table_gejala`;

CREATE TABLE `table_gejala` (
  `id_gejala` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_gejala` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_gejala` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_gejala`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `table_gejala` */

/*Table structure for table `table_history` */

DROP TABLE IF EXISTS `table_history`;

CREATE TABLE `table_history` (
  `id_history` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pasien` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_history`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `table_history` */

insert  into `table_history`(`id_history`,`id_pasien`,`created_at`) values 
(27,28,'2020-10-25 05:37:52'),
(28,29,'2020-10-25 05:38:21'),
(29,30,'2020-10-25 05:45:19'),
(30,31,'2020-10-25 05:45:49'),
(31,32,'2020-10-25 06:22:25'),
(32,33,'2020-10-25 06:24:26');

/*Table structure for table `table_history_gejala` */

DROP TABLE IF EXISTS `table_history_gejala`;

CREATE TABLE `table_history_gejala` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode_gejala` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_history` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `table_history_gejala` */

insert  into `table_history_gejala`(`id`,`kode_gejala`,`id_history`) values 
(6,'P003',32);

/*Table structure for table `table_penyakit` */

DROP TABLE IF EXISTS `table_penyakit`;

CREATE TABLE `table_penyakit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kode_penyakit` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_penyakit` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi_penyakit` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `table_penyakit` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`level`,`email`,`created_at`,`updated_at`) values 
(1,'dimpujlen','$2y$10$nsjdrd/hNgzOiTgo1OhXAOA4VFtWMFm2L65Wu50XR5EmX5Hv0qlzC','admin','dimpujlen@gmail.com','2020-10-24 19:44:18','2020-10-24 19:44:18'),
(2,'admin','$2y$10$Zh4LINaFx0/HdYCSSDSvxu.qxMlB43WIUlqkiYO7z2W1QfZrE9.5i','admin','admin@gmail.com','2020-10-25 06:32:19','2020-10-25 06:32:19'),
(3,'dimas','$2y$10$6S5ZBhXT5SxEf.ejci5Y4.lkC/YAwBXw6pqfzoXzhiVF6m98jJM5W','admin','dimass@gmail.com','2020-10-25 06:48:59','2020-10-25 06:48:59'),
(4,'admin','$2y$10$2j7X7Goz1bim1o6DyJXJ4./OQGSHcM0Yx6zUdqNHvWLW9dfAxsEAe','admin','dimas@gmail.com','2020-10-25 06:57:07','2020-10-25 06:57:07');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
