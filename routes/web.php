<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

// Route::get('/maneh', function () {
//     return view('auth.login');
// });

Auth::routes();
Route::post('login', 'Auth\LoginController@login')->name('sendlogin');
Route::post('register', 'Auth\LoginController@register')->name('kirim_regis');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'admin','middleware' => ['admin']], function () {
    Route::get('/', 'Admin\AdminController@index')->name('landing_admin');
    Route::get('/tampilgejala', 'Admin\AdminController@getGejala')->name('gejala');
    Route::post('/kelgejala', 'Admin\AdminController@postGejala')->name('kelolagejala');
    Route::get('/tampilgejala/{id_gejala}', 'Admin\AdminController@postDGejala')->name('delgel');

    Route::get('/tampilPenyakit', 'Admin\AdminController@getPenyakit')->name('penyakit');
    Route::post('/kelpenyakit', 'Admin\AdminController@postPenyakit')->name('kelolaPenyakit');
    Route::get('/tampilPenyakit/{id}', 'Admin\AdminController@postDPenyakit')->name('delpenyakit');

    Route::get('/tampilmapping', 'Admin\AdminController@getMapping')->name('mapping');
    Route::post('/kelMap', 'Admin\AdminController@postMapping')->name('kelMap');
    Route::get('/tampilmapping/{id_mapping}', 'Admin\AdminController@postDMapping')->name('delMap');


});

// proses dianogsa
Route::get('/biodata', 'diagnosa\diagnosaController@Biodata')->name('bio');
Route::get('/awal', 'diagnosa\diagnosaController@awal')->name('awal');
Route::post('/diagnosa', 'diagnosa\diagnosaController@postDataDiri')->name('diagnosa');
Route::post('/postJawaban', 'diagnosa\diagnosaController@postJawaban')->name('postJawaban');
Route::get('/soal', 'diagnosa\diagnosaController@getSoal')->name('soal');
Route::get('/soal/{id_history}', 'diagnosa\diagnosaController@getHasil')->name('gethasil');
Route::get('/soal/rawat/{id_penyakit}', 'diagnosa\diagnosaController@getPerawatan')->name('perawatan');



